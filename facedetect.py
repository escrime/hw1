from __future__ import print_function
import cv2
import numpy as np
import os

# Should self-collected dataset be used?
altData = True

# Should images with bounding boxes and scores be created?
outputImages = True

# The trained classifier
classifier = '/Users/garrettmcgrath/Downloads/opencv-3.0.0/data/haarcascades/haarcascade_frontalface_default.xml'

# Cascade Classifier parameters
scaleFactor = 1.1
minNeighbors = 3
flags = 0
minSize = (0, 0)
maxSize = (0, 0)

# Creates the classifier
cascade = cv2.CascadeClassifier(classifier)

# Uses self-collected data
if altData:
    fout = open("out/self-out.txt", "w+")
    for i in range(1, 51):
        line = str(i)
        print(line, file=fout)
        img = cv2.imread("dataset/" + line + ".jpg", cv2.IMREAD_COLOR)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        rects, _, levelWeights = cascade.detectMultiScale3(gray, scaleFactor, minNeighbors, flags, minSize, maxSize, outputRejectLevels=True)
        print(len(rects), file=fout)
        for rect, weight in zip(rects, levelWeights):
            print(" ".join([str(num) for num in rect] + [str(weight[0])]), file=fout)
            if outputImages:
                cv2.rectangle(img, (rect[0], rect[1]), (rect[0] + rect[2], rect[1] + rect[3]), (255, 0, 0), 3)
                cv2.putText(img, str(weight[0]), (rect[0], rect[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
        if outputImages:
            path = "out/images/" + line + ".jpg"
            if not os.path.exists(os.path.dirname(path)):
                os.makedirs(os.path.dirname(path))
            cv2.imwrite(path, img)
else:
    # Loops through each of the 10 folds and attempts to detect faces
    for i in range(1, 11):
        fold = str(i).zfill(2)
        fin = open("FDDB-folds/FDDB-fold-" + fold + ".txt", "r")
        fout = open("out/fold-" + fold + "-out.txt", "w+")
        for line in fin:
            line = line.rstrip()
            print(line, file=fout)
            img = cv2.imread("originalPics/" + line + ".jpg", cv2.IMREAD_COLOR)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            rects, _, levelWeights = cascade.detectMultiScale3(gray, scaleFactor, minNeighbors, flags, minSize, maxSize, outputRejectLevels=True)
            print(len(rects), file=fout)
            for rect, weight in zip(rects, levelWeights):
                print(" ".join([str(num) for num in rect] + [str(weight[0])]), file=fout)
                if outputImages:
                    cv2.rectangle(img, (rect[0], rect[1]), (rect[0] + rect[2], rect[1] + rect[3]), (255, 0, 0), 3)
                    cv2.putText(img, str(weight[0]), (rect[0], rect[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
            if outputImages:
                path = "out/images/" + line + ".jpg"
                if not os.path.exists(os.path.dirname(path)):
                    os.makedirs(os.path.dirname(path))
                cv2.imwrite(path, img)